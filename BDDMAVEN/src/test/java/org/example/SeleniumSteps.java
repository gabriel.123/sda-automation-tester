package org.example;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;

public class SeleniumSteps {

    WebDriver driver;

    @Given("Website is loaded")
    public void loadWebsite (){
        System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
        this.driver = new ChromeDriver();
        this.driver.get("https://www.google.com");
    }

    @When("I type in the word mancare")
    public void readWebsite () throws InterruptedException {
        WebElement searchField = driver.findElement(By.name("q"));
        Thread.sleep(1000);
        searchField.sendKeys("mancare");
        Thread.sleep(1000);
        searchField.submit();
    }

    @Then("The webpage should show results")
    public void checkTitle () {
        String title = this.driver.getTitle();
        assertEquals("Vacation Rentals, Homes, Experiences & Places - Airbnb",title);

    }

    @Then("Check search results")
    public void checkSearchResults () {
//        String title = this.driver.getTitle();
//        assertEquals("Vacation Rentals, Homes, Experiences & Places - Airbnb",title);

    }


    @And("Close browser window")
    public void closeWindow () throws InterruptedException {
        Thread.sleep(1000);
        this.driver.close();
    }

    @When("I type the word (.*)$")
    public void iTypeTheWordCuvantulCautat(String cuvant) throws InterruptedException {
        WebElement searchField = driver.findElement(By.name("q"));
        Thread.sleep(3000);
        searchField.sendKeys(cuvant);
        Thread.sleep(1000);
        searchField.submit();
    }

}
