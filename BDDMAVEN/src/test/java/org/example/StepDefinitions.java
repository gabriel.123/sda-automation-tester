package org.example;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.*;

public class StepDefinitions {

    @Before
    public void testHook(){
        System.out.println("Driver initialisation");
    }

    @After
    public void closeConnection(){
        System.out.println("Connection closed");
    }

    @Given ("The user is logged in")
    public void logare() {
        System.out.println("The user is logged in"); }

    @Given ("{string} join the conference")
    public void joinConf(String name) {
        System.out.println(name + " join the workshop");}

    @And("The mic button is displayed")
    public void enableMic() {
        System.out.println("The mic button is displayed"); }

    @When("The user click Share screen button")
    public void sharingScreen() {
        System.out.println("The user click Share screen button"); }

    @When("I press the people button")
    public void peopleButton() {
        System.out.println("I press the people button"); }

    @And("The user clicks the mic button")
    public void micButton() {
        System.out.println("The user clicks the mic button"); }

    @Then("I will be able to see the people present on the conference")
    public void peoplePresent() {
        System.out.println("I will be able to see the people present on the conference");
    }

    @Then("The sharing option menu should be displayed")
    public void clickOptionSharing() {
        System.out.println("The sharing option menu should be displayed");
    }

    @Then("Danut should be on the list")
    public void listOfPeople () {
        System.out.println("Danut is online");
    }

    @And ("<int> should be displayed")
    public void cnpCheck(int cnp) {
        System.out.println( cnp + " is displayed");
    }


}
