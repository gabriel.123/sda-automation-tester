package org.example.page;

import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import selenium.BaseTest;

@Getter
public class FormPage extends BaseTest  {
    WebDriver driver;

    public FormPage (WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "first-name")
    private  WebElement firstNameInput ;

    private  WebElement lastNameInput = this.driver.findElement(By.id("last-name"));
    private  WebElement jobTitleInput = this.driver.findElement(By.id("job-title"));
    private  WebElement educationRadioButtonHighSchool = this.driver.findElement(By.id("radio-button-1"));
    private  WebElement educationRadioButtonHCollage = this.driver.findElement(By.id("radio-button-2"));
    private  WebElement educationRadioButtonGradSchool = this.driver.findElement(By.id("radio-button-3"));
    private  WebElement sexcheckboxmale = this.driver.findElement(By.id("checkbox-1"));
    private  WebElement sexcheckboxfemale = this.driver.findElement(By.id("checkbox-2"));
    private  WebElement sexcheckboxunknown = this.driver.findElement(By.id("checkbox-3"));
    private Select yearsExperincedropdown = (Select) this.driver.findElement(By.id("select-menu"));
    private WebElement dateInput = this.driver.findElement(By.id("datepicker"));

    public void submitForm(WebDriver driver) {
        getFirstNameInput().sendKeys("Ionel");
        getLastNameInput().sendKeys("Ionelule");


    }


}
