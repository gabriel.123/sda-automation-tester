import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.example.page.FormPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.BaseTest;


public class FormStepsImplementation  {
    WebDriver driver;
    FormPage formpage = new FormPage(driver);

    @Given("the user accesses the form")
    public void theUserAccessesTheForm() {
        System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
        this.driver = new ChromeDriver();
        driver.get("https://formy-project.herokuapp.com/form");
    }

    @And("he fills his personal data")
    public void heFillsHisPersonalData() {
        formpage.getFirstNameInput().sendKeys("Ion");

    }

    @When("he submits the form")
    public void heSubmitsTheForm() {
    }

    @Then("the user is informed that he successfully submitted")
    public void theUserIsInformedThatHeSuccessfullySubmitted() {
    }
}
