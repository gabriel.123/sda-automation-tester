package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GoogleSearch {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
        WebDriver driver;

        driver = new ChromeDriver();
        driver.get("https://google.com");
        WebElement searchField = driver.findElement(By.name("q"));
        Thread.sleep(1000);
        searchField.sendKeys("mancare");
        Thread.sleep(1000);
        searchField.submit();
        WebElement searchCheck = driver.findElement(By.id("res"));
        assertTrue(searchCheck.isDisplayed());
        Thread.sleep(1000);
        driver.quit();
    }

}



