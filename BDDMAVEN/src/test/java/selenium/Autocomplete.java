package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Autocomplete {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://formy-project.herokuapp.com/autocomplete");

        WebElement address = driver.findElement(By.id("autocomplete"));
        address.sendKeys("Bucuresti", Keys.TAB);

        WebElement streetAddress = driver.findElement(By.id("street_number"));
        streetAddress.sendKeys("strada Vietii", Keys.TAB);

        WebElement streetAddress2 = driver.findElement(By.id("route"));
        streetAddress2.sendKeys("strada Vietii2", Keys.TAB);

        Thread.sleep(2000);
        driver.quit();

    }
}
