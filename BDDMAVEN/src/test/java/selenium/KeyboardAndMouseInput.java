package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class KeyboardAndMouseInput {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://formy-project.herokuapp.com/keypress");
        WebElement searchField = driver.findElement(By.id("name"));
//        searchField.click();
        searchField.sendKeys("Gigel");
        Thread.sleep(2000);
//        WebElement button = driver.findElement(By.id("button"));
//        button.click();
        driver.findElement(By.id("button")).click();
        Thread.sleep(2000);
        driver.quit();


    }

}
