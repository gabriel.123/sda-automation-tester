package selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ScrollToElement {

    WebDriver driver;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
        this.driver = new ChromeDriver();
    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(2000);
        this.driver.quit();
    }

    @Test
    public void scrollToElement() throws InterruptedException {
        this.driver.get("https://formy-project.herokuapp.com/scroll");

        WebElement name = this.driver.findElement(By.id("name"));
        Actions scroll = new Actions(driver);
          Thread.sleep(2000);
        scroll.moveToElement(name);
           Thread.sleep(2000);
        name.sendKeys("Gigel");

        WebElement date = this.driver.findElement(By.id("date"));
           Thread.sleep(2000);
        date.sendKeys("01/01/2021");


    }

}
