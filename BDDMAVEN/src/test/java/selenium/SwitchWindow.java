package selenium;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class SwitchWindow extends BaseTest {

    @Test
    public void myTest() throws InterruptedException {
        this.driver.get("https://formy-project.herokuapp.com/switch-window");
        WebElement newTabButton = this.driver.findElement(By.id("new-tab-button"));
        newTabButton.click();

        String originalHandle = this.driver.getWindowHandle();

        for (String handle1 : this.driver.getWindowHandles()) {
            this.driver.switchTo().window(handle1);
        }

        Thread.sleep(2000);
        this.driver.switchTo().window(originalHandle);
        System.out.println(originalHandle);

        WebElement alertButton = driver.findElement(By.id("alert-button"));
        alertButton.click();

        Alert alert = driver.switchTo().alert();
//        Thread.sleep(2000);  sau urmatoarea comanda:
        this.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        alert.accept();


    }

}
