Feature: Presence of people
  Testing the presence of people

  Scenario Outline: As a student when I click the People section I want to see the students online
  Given "<name>" join the conference
  When I press the People button
  Then "<nickname>" sould be on the list
  And <cnp> should be displayed

  Examples:
    | name    | nickmane | cnp |
    | Danut   | Danut A  | 1   |
    | Daniel  | Daniel   | 2   |
    | Vasile  | Vasile   | 3   |
    | Madalin | Madalin  | 4   |
    | Gabriel | Gabriel  | 5   |
    | Alina   | Alina    | 6   |
    | Alex    | Alex     | 7   |

#  Scenario: As a student when I click the People section I want to see the students name
#    Given Danut join the conference
#    When I press the People button
#    Then Danut should be on the list
#
#  Scenario: As a student when I click the People section I want to see the students name
#    Given Danut join the conference
#    When I press the People button
#    Then Daniel should be on the list
#
#  Scenario: As a student when I click the People section I want to see the students name
#    Given Danut join the conference
#    When I press the People button
#    Then Vasile should be on the list
#
#  Scenario: As a student when I click the People section I want to see the students name
#    Given Danut join the conference
#    When I press the People button
#    Then Madalin should be on the list
#
#
