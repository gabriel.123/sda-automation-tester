Feature: Testing BlueJeans
  As a user when I run the first BDD feature I want to complete without errors

  @smokeTest

  Scenario: Run first BD feature without errors
    Given The user is logged in
    And The mic button is displayed
    When The user click Share screen button
    And The user clicks the mic button
    Then The sharing option menu should be displayed

  Scenario: As a SDA student when I join the conference I want to see the people are online.
      Given The user is logged in
      When I press the people button
      Then I will be able to see the people present on the conference

#  @smokeTest @chat

#  Scenario: As a SDA student I want to be able to write in Chat menu
#      Given The user is logged in
#      When I press the Chat button
#      And I write in Chat Hello and press Enter
#      Then What I wrote should be displayed in chat