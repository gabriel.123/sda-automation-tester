Feature: Testing Settings
  Testing the Settings functions


  Scenario: As a SDA student I want to see the camera device
    Given The user is logged in
    When I press the Settings button
    And I click on the camera option
    Then I should be able to see available devices
