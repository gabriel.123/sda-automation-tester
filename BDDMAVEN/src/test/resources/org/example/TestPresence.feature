Feature: Presence of people
  Testing the presence of people

  Scenario: As a student when I click the People section I want to see the students name
    Given Danut join the conference
    When I press the People button
    Then Danut should be on the list

  Scenario: As a student when I click the People section I want to see the students name
    Given Danut join the conference
    When I press the People button
    Then Daniel should be on the list

  Scenario: As a student when I click the People section I want to see the students name
    Given Danut join the conference
    When I press the People button
    Then Vasile should be on the list

  Scenario: As a student when I click the People section I want to see the students name
    Given Danut join the conference
    When I press the People button
    Then Madalin should be on the list


