Feature: Website title
  I want to test website title with Selenium

  Scenario: As a user I want to check website title so that title check the requirements
    Given Website is loaded
    When I read the title
    Then Title should be correct
    * Close browser window
