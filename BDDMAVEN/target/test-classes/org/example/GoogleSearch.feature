Feature: Search "mancare" on google
  Scenario Outline: I want to search the word "mancare" on google

    Given Website is loaded
    When I type in the word <cuvantulCautat>
    Then Check search results
     * Close browser window

    Examples:
    |cuvantulCautat|
    |masina      |
    |chiuveta    |
    |robinet     |

