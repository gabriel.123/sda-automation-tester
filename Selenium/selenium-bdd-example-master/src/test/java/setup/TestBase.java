package setup;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class TestBase {

    public static WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/windows/chromedriver.exe");

        this.driver = new ChromeDriver();

    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(1000);
        this.driver.quit();
    }
}
