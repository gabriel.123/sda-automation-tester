package page;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

@Getter
public class FormPage {
    WebDriver driver;

    public FormPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private String pageTitle = "Formy";
    private String pageURLPath = "form";

    @FindBy(id = "first-name")
    private WebElement firstNameInput;
    @FindBy(id = "last-name")
    private WebElement lastNameInput;
    @FindBy(id = "job-title")
    private WebElement jobInput;
    @FindBy(id = "select-menu")
    private Select yearsOfExperienceDropdown;

    @FindBy(xpath = "/html/body/div/form/div/div[8]/a")
    private WebElement submitButton;

}
