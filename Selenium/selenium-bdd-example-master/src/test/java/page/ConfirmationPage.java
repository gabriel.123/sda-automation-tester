package page;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Getter
public class ConfirmationPage {

    WebDriver driver;

    public ConfirmationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    private String pageTitle = "Formy";
    private String subscriptionHeaderMessageText = "Thanks for submitting your form";
    private String subscriptionMessageText = "The form was successfully submitted!";

    @FindBy(xpath = "/html/body/div/h1")
    private WebElement headerMessage;
    @FindBy(xpath = "/html/body/div/div")
    private WebElement subscriptionMessageDiv;
}
