package testImplementation;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SimpleTest {

    @Test
    public void simpleTest1() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/windows/chromedriver.exe");
        WebDriver driver = new ChromeDriver(options);
        driver.get("https://www.google.com");
    }
}
