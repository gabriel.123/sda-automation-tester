package testImplementation;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import setup.TestBase;

import java.util.concurrent.TimeUnit;

public class SimpleTestThatExtendsTestBase extends TestBase {

    @Test
    public void doSomething() throws InterruptedException {
        driver.get("https://formy-project.herokuapp.com/switch-window");
        Thread.sleep(2000);


        WebElement newTabButton = driver.findElement(By.id("new-tab-button"));
        newTabButton.click();

        String originalHandle = driver.getWindowHandle();

        for (String handle1 : driver.getWindowHandles()) {
            driver.switchTo().window(handle1);

        }
        Thread.sleep(2000);
        driver.switchTo().window(originalHandle);

        Thread.sleep(2000);
        System.out.println(originalHandle);

        WebElement alertButton = driver.findElement(By.id("alert-button"));
        alertButton.click();

        Alert alert = driver.switchTo().alert();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //Thread.sleep(2000);
        //alert.accept();
        alert.dismiss();

    }
}
