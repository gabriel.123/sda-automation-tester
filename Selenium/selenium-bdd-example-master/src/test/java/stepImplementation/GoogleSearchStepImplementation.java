package stepImplementation;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import setup.TestBase;

public class GoogleSearchStepImplementation {
    @Given("the user accesses Google search page")
    public void accessGooglePage() {
      //  driver.get("https://formy-project.herokuapp.com/form");

    }

    @And("he types the (.*) word$")
    public void typeWordIntoGoogleSearchBar(String keyword) {
    }

    @When("clicks on search button")
    public void clicksOnSearchButton() {
    }

    @Then("all results should be displayed")
    public void showResults() {
    }
}
