package stepImplementation;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import page.ConfirmationPage;
import page.FormPage;

import java.util.concurrent.TimeUnit;

public class FormStepImplementation {

    WebDriver driver;
    FormPage formPage;
    ConfirmationPage confirmationPage;

    @Given("the user accesses the form")
    public void theUserAccessesTheForm() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/windows/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://formy-project.herokuapp.com/form");
    }

    @And("fills in personal data")
    public void fillsInPersonalData() {
        formPage = new FormPage(driver);
        formPage.getFirstNameInput().sendKeys("Tom");
        formPage.getLastNameInput().sendKeys("Sawyer");
    }

    @When("clicks on submit button")
    public void clicksOnSubmitButton() {
        Actions action = new Actions(driver);
        action.moveToElement(formPage.getSubmitButton());
        formPage.getSubmitButton().click();
    }

    @Then("a confirmation message should be received")
    public void aConfirmationMessageShouldBeReceived() {
        confirmationPage = new ConfirmationPage(driver);

        Assert.assertEquals(confirmationPage.getPageTitle(), driver.getTitle());
        assert (confirmationPage.getHeaderMessage().isDisplayed());
        assert (confirmationPage.getSubscriptionMessageDiv().isDisplayed());

        driver.quit();
    }
}
