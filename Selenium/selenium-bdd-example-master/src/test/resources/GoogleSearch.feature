Feature: Search for a keyword on GOOGLE

  Scenario Outline: I want to search for a specific word on GOOGLE
    Given the user accesses Google search page
#    And he types the <word> word
#    When clicks on search button
#    Then all results should be displayed
#    * Close browser window

    Examples:
      | word        |
      | cafea       |
      | serendipity |
      | am obosit   |