Feature: Fill in forms

  Scenario: Filling forms - positive scenario
    Given the user accesses the form
    And fills in personal data
    When clicks on submit button
    Then a confirmation message should be received
