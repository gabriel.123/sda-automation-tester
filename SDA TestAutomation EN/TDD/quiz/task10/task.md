# Unit tests - features 

Select an option that is not a valid feature of unit tests 

a) Run in production 

b) No additional configuration 

c) Repeatable and decidable - every time for the same data we get the same results 

d) Independent of external resources