
# Dependency Injection Pattern

**Dependency Injection** is used for the purpose (select 2 correct answers):

a) Access acceleration to external resources

b) Transferring responsibility for the creation and management of the resource to the outer class

c) Breaks in the security system of the tested program 

d) Placing the designed class in a library made available to the public 

e) Increasing the transparency and readability of the program code"
