# JUnit - control flow 

The **JUnit** framework control flow also includes methods that will prepare the tested program before executing the test and then cleaning up after the test

Indicate the correct order of annotations (for JUnit5) and the number of executions of the methods marked with them

a) 

| @BeforeAll | @BeforeEach | @Test | @AfterEach | @AfterAll |
|:------------|-------------|-------|------------|-----------|
| 1, at the beginning of a test package | n times, before each test | n times | n times, after each test | 1 |

b) 

| @Before | @BeforeClass | @Test | @AfterClass | @After |
|:------------|-------------|-------|------------|-----------|
| 1, at the beginning of a test package | n times before each test | n times | n times, after each test | 1 |

c)

| @BeforeAll | @BeforeEach | @Test | @AfterEach | @AfterAll |
|:------------|-------------|-------|------------|-----------|
| 1, at the beginning of test execution | before each class | n times | after each test class | 1 |
