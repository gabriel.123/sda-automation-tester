# Test Driven Development 

The main assumption of the TDD concept is to write the test first and then the implementation. Select 2 correct answers from the following, best describing the features of the TDD cycle: 

a) In the SCRUM methodology, testers prepare a base of unit tests, which the \ndevelopers team then implements during the sprint 

b) Program development takes place in the cycle: 1. write test 2. write code 3. refactor. Before refactoring, test should pass correctly and at this point it should not be modified until the changes to the production code

c) During refactoring, it is allowed to modify unit tests, because the code may change to a significant degree and existing tests may not to work properly 

d) We implement only those tests that cover the currently developed functionality - according to the YAGNI principle

e) Once written, a unit test should never be modified