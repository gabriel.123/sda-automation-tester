# Assertions 

Assertions stop program execution when the tested condition is not satisfied. Their main use is in test code, where we use assertions to check test conditions. Can the assertion also be used in production code? Select the best answer. 

a) Assertions are not used to control application behavior and should never be used in production code. 

b) Assertions are not used to control normal application behavior and should not be used in this context. However, it is permissible to use them in snippets of code that should not be accessed during normal operation, and we would like to find out that there is some unexpected situation. 

c) Yes, assertions are a legitimate way to control the program and can be used in sub-auction code.