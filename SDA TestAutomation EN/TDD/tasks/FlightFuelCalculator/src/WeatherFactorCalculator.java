public interface WeatherFactorCalculator {

    /**
     * Calculates the factor changing fuel consumption with regards of wind speed.
     *
     * @param windSpeed - wind speed in m/s,
     *                  positive value - wind has the opposite direction as planned flight,
     *                  negative value - wind has the same direction as planned flight
     * @return factor changing fuel consumption. Negative value should decrease consumption, positive
     * - increase
     */
    public double windFactor(double windSpeed);

}
