import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static java.util.Collections.*;

public class SampleGeometry {

    /**
     * Checks, if the number is divisible by the second one
     * @param divident
     * @param divider
     * @return true, if it is divisible, false otherwise
     */
    public boolean isDivisibleBy(int divident, int divider) {
        throw(new UnsupportedOperationException("Not implemented yet (remove this line)"));
    }

    /**
     * Calculates lower number from given two
     * @param noA
     * @param noB
     * @return
     */
    public int getLowerNumber(int noA, int noB) {
        if (noA > noB) {
            return noB;
        } else {
            return noA;
        }
    }

    /**
     * Calculates the biggest number out of given parameters
     */
    public int getMaxNumberOfFour(int noA, int noB, int noC, int noD) {
//        throw(new UnsupportedOperationException("Not implemented yet (remove this line)"));
       Integer [] listaNumere = {noA, noB, noC, noD};
       return max(Arrays.asList(listaNumere));
    }

    /**
     * Calculates the biggest number out of given parameters.
     * For no parameters given it should throw an exception: "There should be at least one value"
     */
    public int getMaxNumber(int... numbers) {
//        throw(new UnsupportedOperationException("Not implemented yet (remove this line)"));
        int[][] listaNumere = {numbers};
        return max(Arrays.asList(listaNumere));
        return Math.max(numbers);
    }

    /**
     * Checks if it is possible to create a triangle out of given segments
     * (The longest segment has to be shorter than sum of remaining two)
     * @param a
     * @param b
     * @param c
     * @return true - can create, false cannot create
     */
    public boolean canCreateTriangle(int a, int b, int c) {
        throw(new UnsupportedOperationException("Not implemented yet (remove this line)"));
    }

    /**
     * Checks if it is possible to create an equilateral triangle out of the given segments
     * @param a
     * @param b
     * @param c
     * @return
     */
    public boolean canCreateEquilateralTriangle(int a, int b, int c) {
        throw(new UnsupportedOperationException("Not implemented yet (remove this line)"));
    }

    /**
     * Checks if it is possible to create an isosceles triangle out of the given segments
     * @param a
     * @param b
     * @param c
     * @return
     */
    public boolean canCreateIsoscelesTriangle(int a, int b, int c) {
        throw(new UnsupportedOperationException("Not implemented yet (remove this line)"));
    }

}