import org.junit.Assert;
import org.junit.Test;

public class TemperatureReaderTests {

  @Test
  public void test_readTemperature_expect_presetvalue() {

    // Arrange
    PresetFakeThermometer thermometer = new PresetFakeThermometer();
    TemperatureReader temperatureReader = new TemperatureReader();  // remove this line after implementation and ucomment the following line
    // TemperatureReader temperatureReader = new TemperatureReader(thermometer);
    double expectedTemperature = 5.0;

    // Act
    thermometer.setPresetTemperature(expectedTemperature);
    double readTemperature = temperatureReader.readTemperature();

    // Assert
    Assert.assertEquals(
            expectedTemperature,
            readTemperature,
            0.001
    );
  }

}