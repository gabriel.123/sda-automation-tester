public class Factorial {
    public static void main(String[] args) {

        int n = 7;
        int x = Factorial.factorial(n);
        System.out.println("Rezultatul este:" + x);
    }

    public static int factorial(int n) {
        int fact = 1;
        for (int i = n; i > 0; i--) {
            fact *= i;
        }
        return fact;
    }


}
