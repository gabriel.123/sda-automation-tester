import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;



public class Shop {

    private String shopName;
    private List<Merchandise> stock;


    public Shop(String shopName) {
        this.shopName = shopName;
        stock = new ArrayList<>();
    }

    /**
     * Shop name - finally the customer wants to recognize the shop
     * @return Shop name
     */
    public String getShopName() {
        return shopName;
    }

    /**
     * Adds merchandise to the list
     * @param merchandiseName merchandise name
     * @param basePrice base price without any sales
     */
    public void addMerchandise(String merchandiseName, double basePrice) {
        if (!merchandiseName.isEmpty()){
        stock.add(new Merchandise(merchandiseName, basePrice)); }
        else throw new IllegalArgumentException("merchandise name cannot be null");

    }

    /**
     * Check if merchandise is on stock
     * @param merchandiseName merchandise name
     * @return true - is on stock, false - not on stock
     */
    public boolean isOnStock(String merchandiseName) {
        return stock.contains(findMerchandiseByName(merchandiseName));
    }

    /**
     * Returns the price of merchandise
     * @param merchandiseName merchandise name
     * @return price of merchandise
     */
    public double getMerchandisePrice(String merchandiseName) {
        return findMerchandiseByName(merchandiseName).getBasePrice();
        //todo: to be implemented

    }

    public void updateMerchandisePrice (String merchandiseName, double newPrice) {
        findMerchandiseByName(merchandiseName).setNewBasePrice(newPrice);
    }

    /**
     * Looks for merchandise by name, ignoring case
     * @param merchandiseName searched merchandise
     * @return merchandise object, if found, null if not found
     */
    private Merchandise findMerchandiseByName(String merchandiseName) {
        // todo: use this method in the others and make it case insensitive
        for (Merchandise merchandiseInStock: stock) {
            if (merchandiseInStock.getName().equals(merchandiseName))
                return merchandiseInStock;
        }
        return null;
    }

}
