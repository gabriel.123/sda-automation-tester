public class ThisKeyword {
    String name;
    String mail;
    int age;
    private String nickName = "Ionut";

    ThisKeyword( int inputAge, String inputName, String inputMail ){
        this.age = inputAge;
        this.name = inputName;
        this.mail = inputMail;
    }

    String getNickName() {
        return this.nickName;
    }

    void setNickName(String nickName) {
        this.nickName = nickName;
        System.out.println(this.nickName);
    }
}
