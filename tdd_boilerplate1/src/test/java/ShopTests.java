import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ShopTests {


    @Test
    public void checkIfShopNameIsCorrectlyAssigned() {
        Shop shop = new Shop("O'Briens Veggies");

        assertEquals("O'Briens Veggies", shop.getShopName());
    }

    @Test
    public void addMerchandiseAndCheckOnStockSuccess() {
        Shop shop = new Shop("test");
        shop.addMerchandise("Onion", 2.1);
        boolean isOnStock = shop.isOnStock("Onion");

        assertTrue(isOnStock);
    }

    @Test
    public void addMerchandiseAndCheckOnStockFail() {
        Shop shop = new Shop("test");
        shop.addMerchandise("Cartof", 4.2);
        boolean isOnStock = shop.isOnStock("Tomato");

        assertFalse(isOnStock);
        //todo: add implementation
    }

    @Test
    public void addMerchandiseAndCheckOnStockCaseInsensitiveSuccess() {
        Shop shop = new Shop("test");

        shop.addMerchandise("Onion", 2.1);
        boolean isOnStock = shop.isOnStock("onion");

        assertTrue(isOnStock);
    }

    @Test
    public void addMerchandiseAndCheckItsPrice() {
        Shop shop = new Shop("Test");
        shop.addMerchandise("Onion", 2.1);
        double price = shop.getMerchandisePrice("Onion");
        assertEquals(2.1, price);
        //todo: add implementation
    }

    @Test
    public void addMerchandiseAndCheckItsPrice_fail() {
        Shop shop = new Shop("Test");
        shop.addMerchandise("Onion", 2.1);
        double price = shop.getMerchandisePrice("Onion");
        assertNotEquals(2.5, price);

    }

    @Test
    public void updateMerchandisePrice() {
        Shop shop = new Shop("test");
        shop.addMerchandise("Tomato", 4.4);
        shop.updateMerchandisePrice("Tomato", 5.7);
        double newPrice = shop.getMerchandisePrice("Tomato");
        assertEquals(5.7, newPrice);

    }

    @Test
    public void updateMerchandisePrice_notFoundMerchandise() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            Shop shop = new Shop("test");
            shop.addMerchandise("Onion", 4.4);
            shop.updateMerchandisePrice("Carrot", 5.7);

            double newPrice = shop.getMerchandisePrice("Carrot");

            assertEquals(5.7, newPrice);

        });
    }

    @Test
    public void addMerchandiseAndCheckNullName() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Shop shop = new Shop("Test");
            shop.addMerchandise("Onion", 2.1);
            boolean isOnStock = shop.isOnStock("");

            assertTrue(isOnStock);
        });
    }

}